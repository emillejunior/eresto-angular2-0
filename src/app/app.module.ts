import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { SalesPage } from '../pages/sales/sales';
import { TablesPage } from '../pages/tables/tables';
import { TakeAwaysPage } from '../pages/take-aways/take-aways';
import { DeliveriesPage } from '../pages/deliveries/deliveries';
import { CateringsPage } from '../pages/caterings/caterings';
import { ReservationsPage } from '../pages/reservations/reservations';
import { ProductsPage } from '../pages/products/products';
import { CategoriesPage } from '../pages/categories/categories';

import { ReportsPage } from '../pages/reports/reports';
import { HistoryPage } from '../pages/history/history';
import { ProductsParentPage } from '../pages/products-parent/products-parent';
import { MembershipPage } from '../pages/membership/membership';
import { SettingsPage } from '../pages/settings/settings';

@NgModule({
  declarations: [
    MyApp,
    SalesPage,
    TablesPage,
    TakeAwaysPage,
    DeliveriesPage,
    CateringsPage,
    ReservationsPage,
    ProductsParentPage,
    ProductsPage,
    CategoriesPage,
    ReportsPage,
    HistoryPage,
    MembershipPage,
    SettingsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SalesPage,
    TablesPage,
    TakeAwaysPage,
    DeliveriesPage,
    CateringsPage,
    ReservationsPage,
    ProductsParentPage,
    ProductsPage,
    CategoriesPage,
    ReportsPage,
    HistoryPage,
    MembershipPage,
    SettingsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
