import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SalesPage } from '../pages/sales/sales';
import { ReportsPage } from '../pages/reports/reports';
import { HistoryPage } from '../pages/history/history';
import { ProductsParentPage } from '../pages/products-parent/products-parent';
import { MembershipPage } from '../pages/membership/membership';
import { SettingsPage } from '../pages/settings/settings';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage:any = SalesPage;

  pages: Array<{title: string, component: any}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Sales', component: SalesPage },
      { title: 'Reports', component: ReportsPage },
      { title: 'History', component: HistoryPage },
      { title: 'Products', component: ProductsParentPage },
      { title: 'Membership', component: MembershipPage },
      { title: 'Settings', component: SettingsPage }
    ];

  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

