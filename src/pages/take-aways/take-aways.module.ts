import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TakeAwaysPage } from './take-aways';

@NgModule({
  declarations: [
    TakeAwaysPage,
  ],
  imports: [
    IonicPageModule.forChild(TakeAwaysPage),
  ],
  exports: [
    TakeAwaysPage
  ]
})
export class TakeAwaysPageModule {}
