// import { Component } from '@angular/core';
import { Component, ViewChild } from '@angular/core';
// import { NavController } from 'ionic-angular';
import { NavController, Nav, Platform } from 'ionic-angular';

import { TablesPage } from '../tables/tables';
import { TakeAwaysPage } from '../take-aways/take-aways';
import { DeliveriesPage } from '../deliveries/deliveries';
import { CateringsPage } from '../caterings/caterings';
import { ReservationsPage } from '../reservations/reservations';


@Component({
	selector: 'page-sales',
	templateUrl: 'sales.html'
})
export class SalesPage {
	@ViewChild(Nav) nav: Nav;

	tab1Root = TablesPage;
	tab2Root = TakeAwaysPage;
	tab3Root = DeliveriesPage;
	tab4Root = CateringsPage;
	tab5Root = ReservationsPage;

	tabs: Array<{title: string, component: any, target: string}>;

	constructor(public navCtrl: NavController) {

	    this.tabs = [
	      { title: 'Tables', component: this.tab1Root, target: 'tables' },
	      { title: 'Take Aways', component: this.tab2Root, target: 'take-aways' },
	      { title: 'Deliveries', component: this.tab3Root, target: 'deliveries' },
	      { title: 'Caterings', component: this.tab4Root, target: 'caterings' },
	      { title: 'Reservations', component: this.tab5Root, target: 'reservations' }
	    ];

	}

	switchTabs(tab) {
		console.log(tab);
    	this.nav.setRoot(tab.component);
	}
}
