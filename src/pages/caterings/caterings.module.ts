import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CateringsPage } from './caterings';

@NgModule({
  declarations: [
    CateringsPage,
  ],
  imports: [
    IonicPageModule.forChild(CateringsPage),
  ],
  exports: [
    CateringsPage
  ]
})
export class CateringsPageModule {}
