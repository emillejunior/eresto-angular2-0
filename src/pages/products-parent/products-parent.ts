import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ProductsPage } from '../products/products';
import { CategoriesPage } from '../categories/categories';

@Component({
	selector: 'page-products-parent',
	templateUrl: 'products-parent.html'
})
export class ProductsParentPage {

	tab1Root = ProductsPage;
	tab2Root = CategoriesPage;


	constructor(public navCtrl: NavController) {

	}

	switchTabs(tab:number) {
		console.log(tab);
	}
}
